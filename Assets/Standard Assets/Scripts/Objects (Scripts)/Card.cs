using UnityEngine;
using System;

namespace CardCreator
{
	public class Card : UIAsset
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}

		[Serializable]
		public class Data : UIAsset.Data
		{
			[SaveAndLoadValue]
			public TextField.Data[] textFieldsData;
			[SaveAndLoadValue]
			public ImageField.Data[] imageFieldsData;

			public Card MakeCard ()
			{
				Card card = ObjectPool.instance.SpawnComponent<Card>(GameManager.instance.cardPrefab.prefabIndex, parent:GameManager.instance.visualsParent);
				card._Data = this;
				return card;
			}
		}
	}
}