using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Extensions;
using UnityEngine.Events;

[RequireComponent(typeof(Selectable))]
[ExecuteInEditMode]
public class _Selectable : MonoBehaviour
{
	public RectTransform rectTrs;
	public Selectable selectable;
	public Canvas canvas;
	public RectTransform canvasRectTrs;
	public float priority;
	public UnityEvent onSelected;
	public UnityEvent onDeselected;

	void OnEnable ()
	{
#if UNITY_EDITOR
		if (!Application.isPlaying)
		{
			if (rectTrs == null)
				rectTrs = GetComponent<RectTransform>();
			if (selectable == null)
				selectable = GetComponent<Selectable>();
			if (canvas == null)
			{
				Transform canvasTrs = GetComponent<Transform>().parent;
				while (canvas == null)
				{
					canvas = canvasTrs.GetComponent<Canvas>();
					canvasTrs = canvasTrs.parent;
				}
			}
			if (canvasRectTrs == null)
			{
				Transform canvasTrs = GetComponent<Transform>().parent;
				while (canvas == null)
				{
					canvas = canvasTrs.GetComponent<Canvas>();
					canvasTrs = canvasTrs.parent;
				}
				canvasRectTrs = canvas.GetComponent<RectTransform>();
			}
			return;
		}
#endif
		UIControlManager.selectables = UIControlManager.selectables.Add(this);
	}

	void OnDisable ()
	{
#if UNITY_EDITOR
		if (!Application.isPlaying)
			return;
#endif
		UIControlManager.selectables = UIControlManager.selectables.Remove(this);
	}
}
