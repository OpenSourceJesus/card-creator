#if UNITY_EDITOR
using UnityEngine;
using System;

namespace CardCreator
{
	[Serializable]
	[RequireComponent(typeof(Canvas))]
	public class RelativeSortingOrder : EditorScript
	{
		public Canvas parentCanvas;
		public Canvas canvas;
		public int sortingOrder;

		public override void OnEnable ()
		{
			if (canvas == null)
				canvas = GetComponent<Canvas>();
			if (parentCanvas == null)
			{
				Transform canvasTrs = GetComponent<Transform>().parent;
				while (parentCanvas == null)
				{
					parentCanvas = canvasTrs.GetComponent<Canvas>();
					canvasTrs = canvasTrs.parent;
				}
			}
		}

		public override void Do ()
		{
			if (parentCanvas == null)
				return;
			if (canvas == null)
				return;
			canvas.overrideSorting = true;
			canvas.sortingOrder = parentCanvas.sortingOrder + sortingOrder;
		}
	}
}
#else
namespace CardCreator
{
	public class RelativeSortingOrder : EditorScript
	{
	}
}
#endif