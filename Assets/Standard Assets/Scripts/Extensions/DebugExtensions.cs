using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using CardCreator;

namespace Extensions
{
	public static class DebugExtensions
	{
		public static void DrawPoint (Vector3 point, float radius, Color color, float duration)
		{
			Debug.DrawLine(point + (Vector3.right * radius), point + (Vector3.left * radius), color, duration);
			Debug.DrawLine(point + (Vector3.up * radius), point + (Vector3.down * radius), color, duration);
			Debug.DrawLine(point + (Vector3.forward * radius), point + (Vector3.back * radius), color, duration);
		}

		public static void DrawRect (Rect rect, Color color, float duration)
		{
			Debug.DrawLine(rect.min, new Vector2(rect.xMin, rect.yMax), color, duration);
			Debug.DrawLine(new Vector2(rect.xMin, rect.yMax), rect.max, color, duration);
			Debug.DrawLine(rect.max, new Vector2(rect.xMax, rect.yMin), color, duration);
			Debug.DrawLine(new Vector2(rect.xMax, rect.yMin), rect.min, color, duration);
		}

		public static void DrawBounds (Bounds bounds, Color color, float duration)
		{
			LineSegment3D[] sides = bounds.GetSides();
			for (int i = 0; i < sides.Length; i ++)
			{
				LineSegment3D side = sides[i];
				Debug.DrawLine(side.start, side.end, color, duration);
			}
		}

		public static void Log (string elementSeperator = ", ", LogType logType = LogType.Info, params object[] data)
		{
			if (logType == LogType.Info)
				Debug.Log(data.ToString(elementSeperator));
			else if (logType == LogType.Warning)
				Debug.LogWarning(data.ToString(elementSeperator));
			else//if (logType == LogType.Error)
				Debug.LogError(data.ToString(elementSeperator));
		}

		public static void Log<T> (string elementSeperator = ", ", LogType logType = LogType.Info, params Data<T>[] data)
		{
			string output = "";
			for (int i = 0; i < data.Length; i ++)
			{
				Data<T> dataPiece = data[i];
				output += dataPiece.value.ToString() + elementSeperator;
			}
			if (logType == LogType.Info)
				Debug.Log(output);
			else if (logType == LogType.Warning)
				Debug.LogWarning(output);
			else//if (logType == LogType.Error)
				Debug.LogError(output);
		}

		public static void Log (in object data, LogType logType = LogType.Info)
		{
			if (logType == LogType.Info)
				Debug.Log(data.ToString());
			else if (logType == LogType.Warning)
				Debug.LogWarning(data.ToString());
			else//if (logType == LogType.Error)
				Debug.LogError(data.ToString());
		}

		public static void DelayedLog (float delay, bool realtime = true, string elementSeperator = ", ", LogType logType = LogType.Info, params object[] data)
		{
			GameManager.Instance.StartCoroutine(DelayedLogRoutine (delay, realtime, elementSeperator, logType, data));
		}

		public static IEnumerator DelayedLogRoutine (float delay, bool realtime = true, string elementSeperator = ", ", LogType logType = LogType.Info, params object[] data)
		{
			if (realtime)
				yield return new WaitForSecondsRealtime(delay);
			else
				yield return new WaitForSeconds(delay);
			Log (elementSeperator, logType, data);
		}

		public static void DelayedLog<T> (float delay, bool realtime = true, string elementSeperator = ", ", LogType logType = LogType.Info, params Data<T>[] data)
		{
			GameManager.Instance.StartCoroutine(DelayedLogRoutine (delay, realtime, elementSeperator, logType, data));
		}

		public static IEnumerator DelayedLogRoutine<T> (float delay, bool realtime = true, string elementSeperator = ", ", LogType logType = LogType.Info, params Data<T>[] data)
		{
			if (realtime)
				yield return new WaitForSecondsRealtime(delay);
			else
				yield return new WaitForSeconds(delay);
			Log (elementSeperator, logType, data);
		}

		public enum LogType
		{
			Info,
			Error,
			Warning
		}
	}
}