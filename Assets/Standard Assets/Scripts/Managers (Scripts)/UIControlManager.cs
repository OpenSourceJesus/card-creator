using UnityEngine;
using UnityEngine.UI;
using Extensions;
using CardCreator;
using TMPro;
using UnityEngine.InputSystem.UI;
using System.Collections.Generic;

public class UIControlManager : SingletonMonoBehaviour<UIControlManager>, IUpdatable
{
	public _Selectable currentSelected;
	public ComplexTimer colorMultiplier;
	public Timer repeatTimer;
	public float angleEffectiveness;
	public float distanceEffectiveness;
	public InputSystemUIInputModule inputModule;
	public static _Selectable[] selectables = new _Selectable[0];
	Vector2 inputDirection;
	Vector2 previousInputDirection;
	_Selectable selectable;
	bool inControlMode;
	bool controllingWithJoystick;
	bool leftClickInput;
	bool previousLeftClickInput;
	TMP_InputField currentInputField;
	ScrollRect currentScrollRect;
	Scrollbar currentScrollbar;

	public override void Awake ()
	{
		base.Awake ();
		GameManager.updatables = GameManager.updatables.Add(this);
		repeatTimer.onFinished += delegate { HandleChangeSelected (angleEffectiveness, distanceEffectiveness, false); ControlSelected (); };
	}

	void OnDestroy ()
	{
		GameManager.updatables = GameManager.updatables.Remove(this);
		repeatTimer.onFinished -= delegate { HandleChangeSelected (angleEffectiveness, distanceEffectiveness, false); ControlSelected (); };
	}

	public void DoUpdate ()
	{
		leftClickInput = InputManager.LeftClickInput;
		if (currentSelected != null)
		{
			if (!CanSelectSelectable(currentSelected))
			{
				ColorSelected (currentSelected, 1);
				HandleChangeSelected (angleEffectiveness, distanceEffectiveness, true);
			}
			ColorSelected (currentSelected, colorMultiplier.GetValue());
			HandleMouseInput (true);
			HandleMovementInput ();
			HandleSubmitSelected ();
		}
		else
			HandleChangeSelected (angleEffectiveness, distanceEffectiveness, true);
		HandleInputModule ();
		previousLeftClickInput = leftClickInput;
	}

	bool CanSelectSelectable (_Selectable selectable)
	{
		return selectables.Contains(selectable) && selectable.selectable.IsInteractable() && selectable.canvas.enabled;
	}

	bool IsMousedOverSelectable (_Selectable selectable)
	{
		return IsMousedOverRectTransform(selectable.rectTrs, selectable.canvas, selectable.canvasRectTrs);
	}

	bool IsMousedOverRectTransform (RectTransform rectTrs, Canvas canvas, RectTransform canvasRectTrs)
	{
		if (canvas.renderMode == RenderMode.ScreenSpaceOverlay || canvas.worldCamera == null)
			return rectTrs.GetRectInCanvasNormalized(canvasRectTrs).Contains(canvasRectTrs.GetWorldRect().ToNormalizedPosition(InputManager.MousePosition));
		else
			return rectTrs.GetRectInCanvasNormalized(canvasRectTrs).Contains(canvasRectTrs.GetWorldRect().ToNormalizedPosition(canvas.worldCamera.ScreenToWorldPoint(InputManager.MousePosition)));
	}

	void HandleMouseInput (bool forceChange = false)
	{
		if (!leftClickInput && previousLeftClickInput && !controllingWithJoystick)
			inControlMode = false;
		List<_Selectable> potentialNextSelected = new List<_Selectable>();
		for (int i = 0; i < selectables.Length; i ++)
		{
			_Selectable selectable = selectables[i];
			if (IsMousedOverSelectable(selectable) && CanSelectSelectable(selectable))
				potentialNextSelected.Add(selectable);
		}
		HandleChangeSelected (potentialNextSelected.ToArray(), forceChange: forceChange);
		if (leftClickInput)
		{
			_Slider slider = currentSelected.GetComponent<_Slider>();
			if (slider != null)
			{
				Vector2 closestPointToMouseCanvasNormalized = new Vector2();
				if (currentSelected.canvas.renderMode == RenderMode.ScreenSpaceOverlay)
				{
					if (selectable != null)
						closestPointToMouseCanvasNormalized = slider.slidingArea.GetRectInCanvasNormalized(selectable.canvasRectTrs).ClosestPoint(slider.canvasRectTrs.GetWorldRect().ToNormalizedPosition(InputManager.MousePosition));
				}
				else if (selectable != null)
					closestPointToMouseCanvasNormalized = slider.slidingArea.GetRectInCanvasNormalized(selectable.canvasRectTrs).ClosestPoint(slider.canvasRectTrs.GetWorldRect().ToNormalizedPosition(selectable.canvas.worldCamera.ScreenToWorldPoint(InputManager.MousePosition)));
				float normalizedValue = slider.slidingArea.GetRectInCanvasNormalized(slider.canvasRectTrs).ToNormalizedPosition(closestPointToMouseCanvasNormalized).x;
				slider.slider.value = Mathf.Lerp(slider.slider.minValue, slider.slider.maxValue, normalizedValue);
				if (slider.snapValues.Length > 0)
					slider.slider.value = MathfExtensions.GetClosestNumber(slider.slider.value, slider.snapValues);
			}
		}
	}

	void HandleMovementInput ()
	{
		if (currentInputField != null)
			return;
		inputDirection = InputManager.UIMovementInput;
		if (inputDirection.magnitude > InputManager.Settings.defaultDeadzoneMin)
		{
			if (previousInputDirection.magnitude <= InputManager.Settings.defaultDeadzoneMin)
			{
				HandleChangeSelected (angleEffectiveness, distanceEffectiveness, false);
				ControlSelected ();
				repeatTimer.Reset ();
				repeatTimer.Start ();
			}
		}
		else
			repeatTimer.Stop ();
		previousInputDirection = inputDirection;
	}

	void HandleChangeSelected (float angleEffectiveness = 0, float distanceEffectiveness = 0, bool forceChange = false)
	{
		List<_Selectable> otherSelectables = new List<_Selectable>(selectables);
		otherSelectables.Remove(currentSelected);
		HandleChangeSelected (otherSelectables.ToArray(), angleEffectiveness, distanceEffectiveness, forceChange);
	}

	void HandleChangeSelected (_Selectable[] selectables, float angleEffectiveness = 0, float distanceEffectiveness = 0, bool forceChange = false)
	{
		if (selectables.Length == 0 || inControlMode)
			return;
		float selectableAttractiveness;
		_Selectable nextSelected = selectables[0];
		float highestSelectableAttractiveness = GetAttractivenessOfSelectable(nextSelected, angleEffectiveness, distanceEffectiveness);
		for (int i = 1; i < selectables.Length; i ++)
		{
			_Selectable selectable = selectables[i];
			selectableAttractiveness = GetAttractivenessOfSelectable(selectable, angleEffectiveness, distanceEffectiveness);
			if (selectableAttractiveness >= highestSelectableAttractiveness)
			{
				highestSelectableAttractiveness = selectableAttractiveness;
				nextSelected = selectable;
			}
		}
		if (forceChange || currentSelected == null || highestSelectableAttractiveness >= GetAttractivenessOfSelectable(currentSelected))
			ChangeSelected (nextSelected);
	}

	void ChangeSelected (_Selectable selectable)
	{
		if (inControlMode)
			return;
		if (currentSelected != null)
			ColorSelected (currentSelected, 1);
		currentSelected = selectable;
		currentSelected.selectable.Select();
		colorMultiplier.JumpToStart ();
	}

	void HandleSubmitSelected ()
	{
		bool submitInput = InputManager.SubmitInput;
		if (IsMousedOverSelectable(currentSelected) && leftClickInput && !previousLeftClickInput)
		{
			Button button = currentSelected.GetComponent<Button>();
			if (button != null)
				button.onClick.Invoke();
			else
			{
				Toggle toggle = currentSelected.GetComponent<Toggle>();
				if (toggle != null)
					toggle.isOn = !toggle.isOn;
				else
				{
					_Slider slider = currentSelected.GetComponent<_Slider>();
					if (slider != null)
					{
						controllingWithJoystick = submitInput;
						inControlMode = !inControlMode;
					}
				}
			}
		}
	}

	void ControlSelected ()
	{
		if (!inControlMode)
			return;
		_Slider slider = currentSelected.GetComponent<_Slider>();
		if (slider != null)
		{
			slider.indexOfCurrentSnapValue = Mathf.Clamp(slider.indexOfCurrentSnapValue + MathfExtensions.Sign(inputDirection.x), 0, slider.snapValues.Length - 1);
			slider.slider.value = slider.snapValues[slider.indexOfCurrentSnapValue];
		}
	}

	void HandleInputModule ()
	{
		if (currentSelected == null)
			return;
		currentInputField = currentSelected.GetComponent<TMP_InputField>();
		currentScrollRect = currentSelected.GetComponent<ScrollRect>();
		currentScrollbar = currentSelected.GetComponent<Scrollbar>();
		inputModule.enabled = currentInputField != null || currentScrollRect != null || currentScrollbar != null;
	}

	float GetAttractivenessOfSelectable (_Selectable selectable, float angleEffectiveness = 0, float distanceEffectiveness = 0)
	{
		if (!CanSelectSelectable(selectable))
			return -Mathf.Infinity;
		float attractiveness = selectable.priority;
		if (currentSelected != null)
		{
			Vector2 vectorToSelectable = GetVectorToSelectable(selectable);
			float angleAttractiveness = (180f - Vector2.Angle(inputDirection, vectorToSelectable)) * angleEffectiveness;
			float distanceAttractiveness = vectorToSelectable.magnitude * distanceEffectiveness;
			attractiveness += angleAttractiveness - distanceAttractiveness;
		}
		return attractiveness;
	}

	Vector2 GetVectorToSelectable (_Selectable selectable)
	{
		return selectable.rectTrs.GetCenterInCanvasNormalized(selectable.canvasRectTrs) - currentSelected.rectTrs.GetCenterInCanvasNormalized(currentSelected.canvasRectTrs);
	}

	void ColorSelected (_Selectable selectable, float colorMultiplier)
	{
		ColorBlock colors = selectable.selectable.colors;
		colors.colorMultiplier = colorMultiplier;
		selectable.selectable.colors = colors;
	}
}