﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;
using Object = UnityEngine.Object;
using UnityEngine.UI;
using TMPro;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace CardCreator
{
	public class GameManager : SingletonMonoBehaviour<GameManager>, ISaveableAndLoadable
	{
		public GameObject[] registeredGos = new GameObject[0];
		// [SaveAndLoadValue]
		// static string enabledGosString = "";
		// [SaveAndLoadValue]
		// static string disabledGosString = "";
		// [SaveAndLoadValue]
		public CardTemplate cardTemplatePrefab;
		public Card cardPrefab;
		public ImageField imageFieldPrefab;
		public TextField textFieldPrefab;
		[SaveAndLoadValue]
		public List<Asset.Data> assetsData = new List<Asset.Data>();
		// [SaveAndLoadValue]
		// public List<CardTemplate.Data> cardTemplatesData = new List<CardTemplate.Data>();
		// [SaveAndLoadValue]
		// public List<Card.Data> cardsData = new List<Card.Data>();
		// [SaveAndLoadValue]
		// public List<ImageField.Data> imageFieldsData = new List<ImageField.Data>();
		// [SaveAndLoadValue]
		// public List<TextField.Data> textFieldsData = new List<TextField.Data>();
		public Transform dataParent;
		public Transform visualsParent;
		public Canvas canvas;
		public RectTransform canvasRectTrs;
		public Transform chooseCardTemplateButtonsParent;
		public TMP_InputField newCardTemplateWidthInputField;
		public TMP_InputField newCardTemplateHeightInputField;
		public Button newCardTemplateButton;
		public Button newCardButton;
		public Button undoButton;
		public Button redoButton;
		public GameObject newCardPanelGo;
		public GameModifier[] gameModifiers = new GameModifier[0];
		public static Dictionary<string, GameModifier> gameModifierDict = new Dictionary<string, GameModifier>();
		public static bool paused;
		public static IUpdatable[] updatables = new IUpdatable[0];
		public static int framesSinceLevelLoaded;
		public static bool isQuittingGame;

		public override void Awake ()
		{
			base.Awake ();
			if (instance != this)
				return;
			gameModifierDict.Clear();
			for (int i = 0; i < gameModifiers.Length; i ++)
			{
				GameModifier gameModifier = gameModifiers[i];
				gameModifierDict.Add(gameModifier.name, gameModifier);
			}
			SceneManager.sceneLoaded += OnSceneLoaded;
		}

		void OnDestroy ()
		{
			if (instance == this)
				SceneManager.sceneLoaded -= OnSceneLoaded;
		}
		
		void OnSceneLoaded (Scene scene = new Scene(), LoadSceneMode loadMode = LoadSceneMode.Single)
		{
			StopAllCoroutines();
			framesSinceLevelLoaded = 0;
		}

		void Update ()
		{
			for (int i = 0; i < updatables.Length; i ++)
			{
				IUpdatable updatable = updatables[i];
				updatable.DoUpdate ();
			}
			// if (Time.deltaTime > 0)
			// 	Physics.Simulate(Time.deltaTime);
			if (ObjectPool.Instance != null && ObjectPool.instance.enabled)
				ObjectPool.instance.DoUpdate ();
			InputSystem.Update ();
			framesSinceLevelLoaded ++;
		}

		public Asset MakeAsset (Asset asset)
		{
			asset = ObjectPool.instance.SpawnComponent<Asset>(asset.prefabIndex);
			CardTemplate cardTemplate = asset as CardTemplate;
			if (cardTemplate != null)
				cardTemplate.trs.SetParent(null);
			else
			{
				Card card = asset as Card;
				if (card != null)
					card.trs.SetParent(visualsParent);
				else
				{
					ImageField imageField = asset as ImageField;
					if (imageField != null)
						imageField.trs.SetParent(dataParent);
					else
					{
						TextField textField = asset as TextField;
						if (textField != null)
							textField.trs.SetParent(dataParent);
					}
				}
			}
			return asset;
		}

		public void _MakeAsset (Asset asset)
		{
			if (instance != this)
			{
				instance.MakeAsset (asset);
				return;
			}
			MakeAsset (asset);
		}

		public void OnSetNewCardTemplateInfo ()
		{
			if (instance != this)
			{
				instance.OnSetNewCardTemplateInfo ();
				return;
			}
			newCardTemplateButton.interactable = !string.IsNullOrEmpty(newCardTemplateWidthInputField.text) && !string.IsNullOrEmpty(newCardTemplateWidthInputField.text);
		}

		public void MakeCardTemplate ()
		{
			if (instance != this)
			{
				instance.MakeCardTemplate ();
				return;
			}
			CardTemplate cardTemplate = (CardTemplate) MakeAsset(cardTemplatePrefab);
			cardTemplate._Data = new CardTemplate.Data();
			cardTemplate._Data.rect.width = float.Parse(newCardTemplateWidthInputField.text);
			cardTemplate._Data.rect.height = float.Parse(newCardTemplateHeightInputField.text);
		}

		public void Undo ()
		{

		}

		public void Redo ()
		{
			
		}

		public void ClearData ()
		{
			PlayerPrefs.DeleteAll();
			AccountManager.CurrentAccount.score = 0;
			_SceneManager.instance.LoadSceneWithoutTransition (0);
		}
		
		public void Quit ()
		{
			Application.Quit();
		}

		void OnApplicationQuit ()
		{
			SaveAndLoadManager.instance.Save ();
			// PlayerPrefs.DeleteAll();
			isQuittingGame = true;
		}

		public void ToggleGameObjectActive (GameObject go)
		{
			go.SetActive(!go.activeSelf);
		}

		public static void Log (object obj)
		{
			print(obj);
		}

		public static void DestroyImmediate (Object obj)
		{
			Object.DestroyImmediate(obj);
		}
		
#if UNITY_EDITOR
		public static void DestroyOnNextEditorUpdate (Object obj)
		{
			EditorApplication.update += () => { if (obj == null) return; DestroyObject (obj); };
		}

		static void DestroyObject (Object obj)
		{
			if (obj == null)
				return;
			EditorApplication.update -= () => { DestroyObject (obj); };
			DestroyImmediate(obj);
		}
#endif
		
		public static bool ModifierExistsAndIsActive (string name)
		{
			GameModifier gameModifier;
			if (gameModifierDict.TryGetValue(name, out gameModifier))
				return gameModifier.isActive;
			else
				return false;
		}

		public static bool ModifierIsActive (string name)
		{
			return gameModifierDict[name].isActive;
		}

		public static bool ModifierExists (string name)
		{
			return gameModifierDict.ContainsKey(name);
		}

		[Serializable]
		public class GameModifier
		{
			public string name;
			public bool isActive;
		}
	}
}