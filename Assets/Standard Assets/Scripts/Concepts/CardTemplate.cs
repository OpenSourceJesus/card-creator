using UnityEngine;
using System;
using UnityEngine.UI;
using TMPro;

namespace CardCreator
{
	public class CardTemplate : UIAsset
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public Transform chooseButtonTrs;
		public TMP_Text chooseButtonText;

		public override void Start ()
		{
			base.Start ();
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				Button chooseButton = GetComponentInChildren<Button>();
				if (chooseButton != null)
				{
					if (chooseButtonTrs == null)
						chooseButtonTrs = chooseButton.GetComponent<Transform>();
					if (chooseButtonText == null)
						chooseButtonText = chooseButton.GetComponentInChildren<TMP_Text>();
				}
				return;
			}
#endif
		}

		[Serializable]
		public class Data : UIAsset.Data
		{
			[SaveAndLoadValue]
			public Card.Data data;

			public override object MakeAsset ()
			{
				CardTemplate cardTemplate = ObjectPool.instance.SpawnComponent<CardTemplate>(GameManager.instance.cardTemplatePrefab.prefabIndex);
				cardTemplate._Data = this;
				return cardTemplate;
			}
		}
	}
}