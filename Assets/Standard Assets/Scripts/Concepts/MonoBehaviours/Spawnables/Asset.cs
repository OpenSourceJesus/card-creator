using UnityEngine;
using System;

namespace CardCreator
{
	[Serializable]
	public class Asset : Spawnable
	{
		public object data;
		public Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}

		[Serializable]
		public class Data : ISaveableAndLoadable
		{
			[SaveAndLoadValue]
			public string name;

			public virtual object MakeAsset ()
			{
				throw new NotImplementedException();
			}
		}
	}
}