using UnityEngine;
using TMPro;
using System;

namespace CardCreator
{
	public class TextField : UIAsset
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public TMP_InputField inputField;
		public Transform textTrs;
		public TMP_InputField fontPathInputField;

		public override void Start ()
		{
			base.Start ();
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				if (inputField == null)
					inputField = GetComponentInChildren<TMP_InputField>();
				if (fontPathInputField == null)
					fontPathInputField = GetComponentsInChildren<TMP_InputField>()[1];
				return;
			}
#endif
			textTrs.SetParent(GameManager.instance.visualsParent);
			if (_Data == null)
				_Data = new Data();
		}

		public void SetTextOfData ()
		{
			_Data.text = inputField.text;
		}

		public void SetTextFromData ()
		{
			inputField.text = _Data.text;
		}

		public void SetFontPathOfData ()
		{
			_Data.fontPath = fontPathInputField.text;
		}

		public void SetFontPathFromData ()
		{
			fontPathInputField.text = _Data.fontPath;
		}

		[Serializable]
		public class Data : UIAsset.Data
		{
			[SaveAndLoadValue]
			public string text;
			[SaveAndLoadValue]
			public string fontPath;
			[SaveAndLoadValue]
			public TMP_FontAsset fontAsset;

			public override object MakeAsset ()
			{
				TextField textField = ObjectPool.instance.SpawnComponent<TextField>(GameManager.instance.textFieldPrefab.prefabIndex, parent:GameManager.instance.dataParent);
				textField._Data = this;
				textField.SetTextFromData ();
				textField.SetFontPathFromData ();
				return textField;
			}
		}
	}
}