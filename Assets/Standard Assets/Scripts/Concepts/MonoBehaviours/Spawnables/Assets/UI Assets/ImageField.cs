using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.IO;
using System;

namespace CardCreator
{
	public class ImageField : UIAsset
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public RawImage rawImage;
		public TMP_InputField pathInputField;
		public Transform rawImageTrs;

		public override void Start ()
		{
			base.Start ();
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				if (rawImage == null)
					rawImage = GetComponentInChildren<RawImage>();
				if (pathInputField == null)
					pathInputField = GetComponentInChildren<TMP_InputField>();
				return;
			}
#endif
			rawImageTrs.SetParent(GameManager.instance.visualsParent);
			if (_Data == null)
				_Data = new Data();
		}

		public void SetImagePathOfData ()
		{
			_Data.imagePath = pathInputField.text;
		}

		public void SetImagePathFromData ()
		{
			pathInputField.text = _Data.imagePath;
		}

		public void SetImageDataOfData ()
		{
			_Data.imageData = File.ReadAllBytes(_Data.imagePath);
		}

		public void SetImageDataFromData ()
		{
			Texture2D texture = new Texture2D(0, 0);
			texture.LoadImage(_Data.imageData);
			rawImage.texture = texture;
		}

		[Serializable]
		public class Data : UIAsset.Data
		{
			[SaveAndLoadValue]
			public string imagePath;
			[SaveAndLoadValue]
			public byte[] imageData;

			public override object MakeAsset ()
			{
				ImageField imageField = ObjectPool.instance.SpawnComponent<ImageField>(GameManager.instance.imageFieldPrefab.prefabIndex, parent:GameManager.instance.dataParent);
				imageField._Data = this;
				imageField.SetImagePathFromData ();
				imageField.SetImageDataFromData ();
				return imageField;
			}
		}
	}
}