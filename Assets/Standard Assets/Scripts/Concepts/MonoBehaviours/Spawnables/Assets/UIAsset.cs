using UnityEngine;
using System;

namespace CardCreator
{
	public class UIAsset : Asset
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public _Selectable[] selectables = new _Selectable[0];

		public override void Start ()
		{
			base.Start ();
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				if (selectables.Length == 0)
					selectables = GetComponentsInChildren<_Selectable>();
				return;
			}
#endif
			trs.localScale = Vector3.one;
			for (int i = 0; i < selectables.Length; i ++)
			{
				_Selectable selectable = selectables[i];
				selectable.canvas = GameManager.instance.canvas;
				selectable.canvasRectTrs = GameManager.instance.canvasRectTrs;
			}
		}

		[Serializable]
		public class Data : Asset.Data
		{
			[SaveAndLoadValue]
			public Rect rect;
			[SaveAndLoadValue]
			public float rotation;

			public override object MakeAsset ()
			{
				throw new NotImplementedException();
			}
		}
	}
}